Algorithm Design Course Materials (2010-2012)
=============================================

I created and used these materials as tools to aid me in teaching the Algorithm
Design laboratories while I was a TA for the subject between 2010-2012 at the
Politehnica University of Bucharest.

The archive contains:
* the problem statements for all the lab problems in the 2011-2012
  university year.
* the official solutions I wrote for solving these problems
  (in the codeBase/C++ folder).
* an automatic evaluator I wrote for the second assignment
  (in the codeBase/Assignment2_Evaluator folder) of 2011-2012.

  The assignment asked the students to write an engine that can beat the
  Algorithm Design team's engine at the Connect4 game. You can watch my
  description of how to use the evaluator on YouTube (Part 1[0], and Part 2[1]).

Altough the source code is written exclusively in English, all of the comments,
word documents, and YouTube videos associated with this repository are in
English, the teaching language of the Algorithm Design course at my
undergraduate university.

[0] - https://www.youtube.com/watch?v=o_pWN9Se470
[1] - https://www.youtube.com/watch?v=nkEN1drRG9E
