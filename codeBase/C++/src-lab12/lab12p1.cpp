#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <vector>

#include "VectorIO.h"
#include "Point.h"

#define SQR(a) ((a) * (a))

static const int MaxIterations = 1000;

double squared_euclidean_distance(const Point& left,
                                  const Point& right) {
  return (left.x - right.x) * (left.x - right.x) +
      (left.y - right.y) * (left.y - right.y);
}

/* Selecteaza aleator k puncte din vectorul primit.
 * Se urmareste ca fiecare punct sa fie selectat cu probabilitatea k / n. */
void init_centroids(const std::vector<Point>& points, int k,
                    std::vector<Point>& centroids) {
  std::vector<int> index;
  for (int i = 0; i < points.size(); ++i) {
    index.push_back(i);
  }

  random_shuffle(index.begin(), index.end());

  for (int i = 0; i < k; ++i) {
    centroids.push_back(points[index[i]]);
  }
}

/* Calculeaza cel mai apropiat centroid pentru punct. */
int compute_cluster(const std::vector<Point>& centroids,
                    const Point& point) {
  int min_index = 0;

  /* In min_index trebuie sa puneti indicele centroidului din vectorul
   * "centroids" care este cel mai apropiat de "point". Daca sunt mai multi
   * centroizi la aceeasi distanta de point, puteti intoarce indexul oricaruia
   * dintre ei. */
  for (int i = 0; i < centroids.size(); ++i) {
    if (squared_euclidean_distance(point, centroids[min_index]) >
        squared_euclidean_distance(point, centroids[i])) {
      min_index = i;
    }
  }

  return min_index;
}

void kmeansplusplus(std::vector<Point>& points, unsigned int k)
{
  /* Initializare centroizi. Pentru comoditate, vom folosi tot structura
   * Point, dar vom lasa campul cluster necompletat. */
  std::vector<Point> centroids;
  init_centroids(points, k, centroids);

  std::vector<int> centroid_count(centroids.size());
  for (int step = 0; step < MaxIterations; ++step) {
    /* Vom folosi o variabila booleana pentru a detecta pasul de la care nu se
     * mai efectueaza schimbari in componenta clusterelor. */
    bool done = true;

    /* Asignati toate punctele la cate un centroid. */
    for (int i = 0; i < points.size(); ++i) {
      int centroid_index = compute_cluster(centroids, points[i]);
      done = done && (centroid_index == points[i].cluster);
      points[i].cluster = centroid_index;
    }

    /* Recalculati centroizii. */
    for (int i = 0; i < centroids.size(); ++i) {
      centroids[i] = Point(0, 0);
      centroid_count[i] = 0;
    }

    for (int i = 0; i < points.size(); ++i) {
      centroid_count[points[i].cluster]++;
      centroids[points[i].cluster] += points[i];
    }

    for (int i = 0; i < centroids.size(); ++i) {
      centroids[i] /= (centroid_count[i] != 0 ? centroid_count[i] : 1);
    }
  }
}

int main()
{
  /* Deschidem un fisier si citim din el setul de puncte. */
  std::ifstream in("src-lab12/Kmeans.in");
  int k;
  std::vector<Point> points;
  in >> k >> points;

  std::cout << "Points are: " << std::endl << points << std::endl;

  /* Initializare generator numere aleatoare. */
  srand(time(NULL));

  /* Aplicam KMeans++ pentru a completa clusterele. */
  kmeansplusplus(points, k);

  /* Afisam punctele si clusterele asignate dupa fiecare pas. */
  std::cout << points << std::endl;

  return 0;
}
