public abstract class Player extends Process {
	
	public int nextMove;
	
	public void getNextMove() throws Exception {
	}
	
	public void sendMove(int move) throws Exception {
	}
	
	@Override
	public void destroy() {
	}

	@Override
	public int exitValue() {
		return 0;
	}

}
