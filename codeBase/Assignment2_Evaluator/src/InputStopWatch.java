import java.util.Calendar;


public class InputStopWatch extends Thread {

	long initialSetTime;
	long stepSetTime;
	
	private long timeout;
	
	boolean timedOut;
	boolean failedInput;
	
	Player player;
	byte playerID;
	
	InputStopWatch(long timeout, Player player, byte playerID){
		this.timeout = timeout;
		this.player = player;
		this.playerID = playerID;
		this.failedInput = false;
	}
	
	public void run() {
		timedOut = false;
		initialSetTime = Calendar.getInstance().getTimeInMillis();
		Thread inputThread = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					player.getNextMove();
				} catch (Exception e) {
					failedInput = true;
					e.printStackTrace();
				}
			}
			
		});
		inputThread.start();
		do {
			// The step begins now
			stepSetTime = Calendar.getInstance().getTimeInMillis();
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				System.err.println("Aborting timer...");
			}
			long increment = Calendar.getInstance().getTimeInMillis() - stepSetTime;
			// The step ended now, and the time elapse was increment, which will be added to total final time
			if (playerID == GameMap.FIRST_PLAYER){
				MainFrame.timePlayer1 += increment;
			} else {
				MainFrame.timePlayer2 += increment;
			}
			// Deduce the spent time from the timeout and check if it isn't over
			timeout -= increment;
			if (timeout < 0 && MainFrame.buttons.disableTimeout.isSelected() == false){
				timedOut = true;
				inputThread.interrupt();
			}
		} while(inputThread.isAlive() && timedOut == false);
	}

}
