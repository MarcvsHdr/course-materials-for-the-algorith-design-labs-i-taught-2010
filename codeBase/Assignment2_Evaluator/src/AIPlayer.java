import java.io.*;
import java.util.*;

public class AIPlayer extends Player {

	private Process processInstance;
	
	public AIPlayer(String pathToExecutable, boolean isFirst) throws Exception {
		ProcessBuilder processBuilder = new ProcessBuilder();
		if (pathToExecutable.endsWith(".class")) {
			char separator;
			if (pathToExecutable.startsWith("/")){
				separator = '/';
			} else {
				separator = '\\';
			}
			/* we assume that the path is a Java class -- yeah, we'll see about that */
			processBuilder.command().add("java");
			/* compose the class path */
			String classPath = pathToExecutable.substring(0, pathToExecutable.lastIndexOf(separator));
			processBuilder.command().add("-cp");
			processBuilder.command().add(classPath);
			/* compose the class name */
			String className = pathToExecutable.substring(pathToExecutable.lastIndexOf(separator) + 1, pathToExecutable.length() - ".class".length());
			processBuilder.command().add(className);
			/* compose the parameter */
			processBuilder.command().add((isFirst ? "first" : "second"));
			processInstance = processBuilder.start();
		} else {
			/* we assume that the path is a C/C++ executable */
			processBuilder.command().add(pathToExecutable);
			processBuilder.command().add((isFirst ? "first" : "second"));
			processInstance = processBuilder.start();
		}
		//System.out.println(processBuilder.command().toString());
	}
	
	@Override
	public InputStream getErrorStream() {
		/* a process cannot have access to the error printed by a process, but perhaps they should */
		return null; // TODO -- decide whether the output in the error should be redirected here
	}

	@Override
	public InputStream getInputStream() {
		return processInstance.getInputStream();
	}

	@Override
	public OutputStream getOutputStream() {
		return processInstance.getOutputStream();
	}

	@Override
	public int waitFor() throws InterruptedException {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public void destroy() {
		processInstance.destroy();
	}
	
	@Override
	public int exitValue() {
		return processInstance.exitValue();
	}
	
	@Override
	public void sendMove(int move) throws Exception {
		PrintStream printStream = new PrintStream(processInstance.getOutputStream());
		printStream.println(move);
		printStream.flush();
	}
	
	@Override
	public void getNextMove() throws Exception {
		Scanner s = new Scanner(processInstance.getInputStream());
		this.nextMove = s.nextInt();
	}

}
