import java.io.InputStream;
import java.io.OutputStream;


public class HumanPlayer extends Player {

	HumanPlayer(GameCanvas eventSource){
	}
	
	@Override
	public void destroy() {
		return;
	}

	@Override
	public int exitValue() {
		return 0;
	}

	@Override
	public InputStream getErrorStream() {
		return null;
	}

	@Override
	public InputStream getInputStream() {
		return System.in;
	}

	@Override
	public OutputStream getOutputStream() {
		return System.out;
	}

	@Override
	public int waitFor() throws InterruptedException {
		return 0;
	}
	
	public void getNextMove(){
		// Start a new thread that waits for an input on the game canvas
		GameCanvas.lastClickedColumn = GameCanvas.NONE;
		Thread inputSynchronization = new Thread(new Runnable(){

			@Override
			public void run() {
				while (GameCanvas.lastClickedColumn == GameCanvas.NONE){
					try {
						Thread.sleep(75);
					} catch (InterruptedException e) {
						MainFrame.statsPanel.logText += "Aborting wait for player input!\n";
					}
				}
			}
			
		});
		inputSynchronization.start();
		try {
			inputSynchronization.join();
		} catch (InterruptedException e) {
			MainFrame.statsPanel.logText += "Aborting wait on user input...\n";
		}
		this.nextMove = GameCanvas.lastClickedColumn;
	}
}
