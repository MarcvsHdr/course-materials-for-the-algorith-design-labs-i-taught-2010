import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public class GameCanvas extends Canvas implements MouseListener{
	
	private static final long serialVersionUID = 1L;

	MainFrame owner;
	
	public GameMap gameMap;
	
	public static int lastClickedColumn;
	
	public static final int NONE = -1;
	
	public GameCanvas(MainFrame owner){
		super();
		GameCanvas.lastClickedColumn = NONE;
		this.addMouseListener(this);
		this.owner = owner;
		gameMap = null;
	}
	
	public void setGameMap(GameMap gameMap){
		this.gameMap = gameMap;
	}
	
	public void resetGameMap(){
		this.gameMap = null;
	}
	
	public void paint(Graphics g) {
		// A canvas with no map attached to it defaults to blank
		g.setColor(Color.black);
		g.clearRect(0, 0, GParam.FRAME_WIDTH, GParam.FRAME_HEIGHT);
		if (gameMap != null){
			// This is the name of the sprite which must be rendered
			String targetName = "none";
			// Paint an actual map
			for (int i = 0; i < GParam.MAP_HEIGHT; i++){
				for (int j = 0; j < GParam.MAP_WIDTH; j++){
					// Paint the actual chip picture
					switch (gameMap.gameMap[i][j]){
					case GameMap.EMPTY:
						targetName = "emptySlot";
						break;
					case GameMap.FIRST_PLAYER:
						targetName = "firstPlayerChip";
						break;
					case GameMap.SECOND_PLAYER:
						targetName = "secondPlayerChip";
						break;
					}
					if (targetName != "none"){
						g.drawImage(MainFrame.sprite.get(targetName),j*GParam.SPRITE_WIDTH,i*GParam.SPRITE_HEIGHT, null);
					} else {
						System.err.println("Could not find texture for: (" + i + "," + j +") code: " + gameMap.gameMap[i][j]);
					}
					// Paint any mismatches
					Color highlightColor = gameMap.getHighlight(i,j);
					if (highlightColor != null){
						g.setColor(highlightColor);
						g.drawRect(j*GParam.SPRITE_WIDTH, i*GParam.SPRITE_HEIGHT, GParam.SPRITE_WIDTH, GParam.SPRITE_HEIGHT);
						g.fillRect(j*GParam.SPRITE_WIDTH, i*GParam.SPRITE_HEIGHT, GParam.SPRITE_WIDTH, GParam.SPRITE_HEIGHT);
					}
				}
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		lastClickedColumn = e.getX()/GParam.SPRITE_WIDTH;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
