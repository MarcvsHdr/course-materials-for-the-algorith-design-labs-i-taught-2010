import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;

class GParam{
	// The difficulty of the game
	public static int DIFFICULTY = 0;
	// The size of the actual game map
	public static final int MAP_WIDTH = 9;
	public static final int MAP_HEIGHT = 9;
	// The size of the individual textures
	public static final int SPRITE_WIDTH = 57;
	public static final int SPRITE_HEIGHT = 57;
	// The size of the main window where the game plays
	public static final int FRAME_WIDTH;
	public static final int FRAME_HEIGHT;
	public static final long TIMEOUT_IN_MILIS = 5000;
	// The textures in the application are all characterized by a name
	public static String[] availableTextures = { "emptySlot" , "firstPlayerChip" , "secondPlayerChip" };
	// Virtual path to the human player
	public static String HUMAN_PLAYER_PATH = "Human Player";
	
	static {
		FRAME_WIDTH = SPRITE_WIDTH*MAP_WIDTH;
		FRAME_HEIGHT = SPRITE_HEIGHT*MAP_HEIGHT;	
	}
}

class StatsPanel extends JPanel{

	private static final long serialVersionUID = 1L;
	
	MainFrame owner;
	
	JLabel moveNumber;
	JLabel player1Time;
	JLabel player2Time;
	JTextArea log;
	JScrollPane scrollPane;
	String logText;
	
	StatsPanel(MainFrame owner){
		this.owner = owner;
		buildInstance();
	}
	
	private void buildInstance(){
		this.setLayout(new GridBagLayout());
		GridBagConstraints localConstraints;
		
		moveNumber = new JLabel("Move number: -");
		localConstraints = new GridBagConstraints(0, 0, 200, 20, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2,2,2,2), 0, 0);
		this.add(moveNumber, localConstraints);
		player1Time = new JLabel("Player 1 elapsed time: -");
		localConstraints = new GridBagConstraints(0, 20, 200, 20, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2,2,2,2), 0, 0);
		this.add(player1Time, localConstraints);
		player2Time = new JLabel("Player 2 elapsed time: -");
		localConstraints = new GridBagConstraints(0, 40, 200, 20, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2,2,2,2), 0, 0);
		this.add(player2Time, localConstraints);
		log = new JTextArea("", 30, 40); // TODO The number of line is in fact a magic number
		localConstraints = new GridBagConstraints(0, 60, 200, 50, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(1,1,1,1), 0, 0);
		log.setMaximumSize(new Dimension(300,GParam.FRAME_HEIGHT-70));
		scrollPane = new JScrollPane(log);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		this.add(scrollPane, localConstraints);
	}
}

class CustomMenu extends JMenuBar implements ActionListener {

	private static final long serialVersionUID = 449185584601867436L;
	
    public Map<String,JMenu> menuCollection;

    MainFrame owner;
    
    ButtonGroup group;
    JCheckBoxMenuItem easy;
    JCheckBoxMenuItem medium;
    JCheckBoxMenuItem hard;
    
    CustomMenu(MainFrame owner) {
        super();
        this.owner = owner;
        buildInstance();
    }

    private void buildInstance() {
    	group = new ButtonGroup();
        // Create the menu collection
        menuCollection = new HashMap<String,JMenu>();
        // Local auxiliary variables
        JMenu menu;
        JMenuItem menuItem;
        // The "File" Menu
        menuCollection.put("File", new JMenu("File"));
        menu = menuCollection.get("File");
        menuItem = menu.add(new JMenuItem("Exit"));
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, ActionEvent.ALT_MASK));
        menuItem.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                owner.exitGracefully();
            }
            
        });
        this.add(menu);
        // The "Difficulty" Menu
        menuCollection.put("Difficulty", new JMenu("Difficulty"));
        menu = menuCollection.get("Difficulty");
        easy = new JCheckBoxMenuItem("Level 0", true);
        group.add(easy);
        easy.addActionListener(this);
        menu.add(easy);
        medium = new JCheckBoxMenuItem("Level 1", false);
        group.add(medium);
        medium.addActionListener(this);
        menu.add(medium);
        hard = new JCheckBoxMenuItem("Level 2", false);
        group.add(hard);
        hard.addActionListener(this);
        menu.add(hard);
        this.add(menu);
        // The "About" Menu
        menuCollection.put("Help", new JMenu("Help"));
        menu = menuCollection.get("Help");
        menuItem = menu.add(new JMenuItem("About"));
        menuItem.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(owner, owner.getAboutInformation());
            }

        });
        this.add(menu);
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		if (easy.isSelected()){
			GParam.DIFFICULTY = 0;
		} else if (medium.isSelected()) {
			GParam.DIFFICULTY = 1;
		} else if (hard.isSelected()){
			GParam.DIFFICULTY = 2;
		}
	}

}

class Animatie extends Thread {

	Image img;
	
	public Animatie(){
		img = new BufferedImage(GParam.FRAME_WIDTH,GParam.FRAME_HEIGHT, BufferedImage.TYPE_INT_ARGB);
	}
	
	@Override
	public void run() {
		while (true){
			try {
				Thread.sleep(75);
				MainFrame.depiction.paint(img.getGraphics());
				MainFrame.displayBuffer.getGraphics().drawImage(img, 0, 0, null);
				// Reset the text of the statistics pane
				if (MainFrame.lastSimulatedStep != -1){
					MainFrame.statsPanel.moveNumber.setText("Move number: " + (MainFrame.lastSimulatedStep+1));
					MainFrame.statsPanel.player1Time.setText("Player 1 elapsed time: " + ((double)MainFrame.timePlayer1/1000) + " sec");
					MainFrame.statsPanel.player2Time.setText("Player 2 elapsed time: " + ((double)MainFrame.timePlayer2/1000) + " sec");
					MainFrame.stats.setText("You must click 'Reset' before starting a new game!");
				} else {
					MainFrame.statsPanel.moveNumber.setText("Move number: -");
					MainFrame.statsPanel.player1Time.setText("Player 1 elapsed time: -");
					MainFrame.statsPanel.player2Time.setText("Player 2 elapsed time: -");
					MainFrame.stats.setText("You must click 'Start' to start a new game!");
				}
				MainFrame.statsPanel.log.setText(MainFrame.statsPanel.logText);
				MainFrame.statsPanel.log.setCaretPosition(MainFrame.statsPanel.logText.length());
			} catch (Exception e) {
			}
		}
	}
	
}

class ButtonBar extends JPanel implements ActionListener, Runnable {
	
	public static int WINNER;
	
	public void run() {
		MainFrame.viewerMap = new GameMap();
		MainFrame.depiction.setGameMap(MainFrame.viewerMap);
		MainFrame.lastSimulatedStep = -1;
		MainFrame.timePlayer1 = 0;
		MainFrame.timePlayer2 = 0;
		MainFrame.statsPanel.logText = "";
		// Continue playing till somebody wins or till the game ends in a tie
		WINNER = GameMap.EMPTY;
		do {
			WINNER = takeAStep();
		} while (WINNER == GameMap.EMPTY && MainFrame.lastSimulatedStep + 1 < GParam.MAP_HEIGHT * GParam.MAP_WIDTH);
		// Change the code of the winner according to the protocol
		int ANSWER1 = 12;
		int ANSWER2 = 12; 
		switch(WINNER){
		case GameMap.FIRST_PLAYER:
			ANSWER1 = 10;
			ANSWER2 = 11;
			break;
		case GameMap.SECOND_PLAYER:
			ANSWER1 = 11;
			ANSWER2 = 10;
			break;
		}
		// Announce the winner, if there is anyone left to announce it to
		try {
			MainFrame.player1process.sendMove(ANSWER1); 
		} catch (Exception e) {
			MainFrame.statsPanel.logText += "Failed to notify Player 1 of outcome!\n";
		}
		try {
			MainFrame.player2process.sendMove(ANSWER2); 
		} catch (Exception e) {
			MainFrame.statsPanel.logText += "Failed to notify Player 2 of outcome!\n";
		}
	}
	
	private static final long serialVersionUID = 1L;
	
	JPanel panelBRow0;
	JButton startButton;
	JButton stopButton;
	JButton saveButton;
	JCheckBox disableTimeout;
	JLabel placeHolderBRow0;
	
	JCheckBox player1SourceCheckBox;
	JCheckBox player2SourceCheckBox;
	JTextField player1Path;
	JTextField player2Path;
	JFileChooser fileChooser;
	
	MainFrame owner;
	
	public ButtonBar(MainFrame owner) {
		super();
		this.owner = owner;
		this.setLayout(new GridBagLayout());
		GridBagConstraints localConstraints;
		this.setBorder(new TitledBorder("Control panel:"));
		// Creating the paths
		player1SourceCheckBox = new JCheckBox("Player 1 is AI",false);
		player1SourceCheckBox.addActionListener(this);
		localConstraints = new GridBagConstraints(0,0,30,20,0,0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2,2,2,2),0,0);
		this.add(player1SourceCheckBox, localConstraints);
		player1Path = new JTextField(GParam.HUMAN_PLAYER_PATH);
		player1Path.setEditable(false);
		localConstraints = new GridBagConstraints(30,0,30,20,1,0,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2,2,2,2),0,0);
		this.add(player1Path, localConstraints);
		
		player2SourceCheckBox = new JCheckBox("Player 2 is AI",false);
		player2SourceCheckBox.addActionListener(this);
		localConstraints = new GridBagConstraints(0,20,30,20,0,0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2,2,2,2),0,0);
		this.add(player2SourceCheckBox, localConstraints);
		player2Path = new JTextField(GParam.HUMAN_PLAYER_PATH);
		player2Path.setEditable(false);
		localConstraints = new GridBagConstraints(30,20,30,20,1,0,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2,2,2,2),0,0);
		this.add(player2Path, localConstraints);
		
		// Creating the buttons on BRow0
		panelBRow0 = new JPanel();
		panelBRow0.setLayout(new GridBagLayout());
		startButton = new JButton("Start");
		startButton.addActionListener(this);
		localConstraints = new GridBagConstraints(0,0,30,20,0,0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2,2,2,2),0,0);
		panelBRow0.add(startButton, localConstraints);
		stopButton = new JButton("Reset");
		stopButton.addActionListener(this);
		localConstraints = new GridBagConstraints(30,0,30,20,0,0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2,2,2,2),0,0);
		panelBRow0.add(stopButton, localConstraints);
		saveButton = new JButton("Save Log");
		saveButton.addActionListener(this);
		localConstraints = new GridBagConstraints(60,0,30,20,1,0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2,2,2,2),0,0);
		panelBRow0.add(saveButton,localConstraints);
		disableTimeout = new JCheckBox("Disable timeout (DEBUG mode)", false);
		disableTimeout.addActionListener(this);
		localConstraints = new GridBagConstraints(90,0,30,20,1,0,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2,2,2,2),0,0);
		panelBRow0.add(disableTimeout, localConstraints);
		placeHolderBRow0 = new JLabel("");
		localConstraints = new GridBagConstraints(120,0,30,20,1,0,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2,2,2,2),0,0);
		panelBRow0.add(placeHolderBRow0, localConstraints);
		
		localConstraints = new GridBagConstraints(0,40,GParam.FRAME_WIDTH, 20, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0);
		this.add(panelBRow0, localConstraints);
		
		// Make the file chooser
		fileChooser = new JFileChooser();
	}

	private void resetAll(String s) {
		if (s != ""){
			JOptionPane.showMessageDialog(null, s);
		}
		// Kills the thread that runs the game
		if (MainFrame.run != null){
			MainFrame.run.interrupt();
			MainFrame.run = null;
		}
		// Kills the processes of the players
		if (MainFrame.player1process != null){
			MainFrame.player1process.destroy();
		}
		if (MainFrame.player2process != null){
			MainFrame.player2process.destroy();
		}
		// Destroy the viewer map and reset the steps taken
		MainFrame.viewerMap = null;
		MainFrame.lastSimulatedStep = -1;
		MainFrame.depiction.setGameMap(null);
	}
	
	private int takeAStep() {
		MainFrame.lastSimulatedStep++;
		int moveInt = -1;
		String playerName;
		String otherPlayerName;
		Player playerProcess;
		Player otherPlayerProcess;
		byte playerID;
		byte otherPlayerID;
		if (MainFrame.lastSimulatedStep % 2 == 0){
			playerName = "Player 1";
			otherPlayerName = "Player 2";
			playerProcess = MainFrame.player1process;
			otherPlayerProcess = MainFrame.player2process;
			playerID = GameMap.FIRST_PLAYER;
			otherPlayerID = GameMap.SECOND_PLAYER; 
		} else {
			playerName = "Player 2";
			otherPlayerName = "Player 1";
			playerProcess = MainFrame.player2process;
			otherPlayerProcess = MainFrame.player1process;
			playerID = GameMap.SECOND_PLAYER;
			otherPlayerID = GameMap.FIRST_PLAYER;
		}
		// Step 1: Try to obtain the next move from the player
		try{
			MainFrame.statsPanel.logText += "Wating for " + playerName + " to send move... \n"; 
			InputStopWatch timedInput = new InputStopWatch(GParam.TIMEOUT_IN_MILIS, playerProcess, playerID);
			long startTime = Calendar.getInstance().getTimeInMillis();
			timedInput.start();
			timedInput.join();
			long stopTime = Calendar.getInstance().getTimeInMillis();
			/* see if the pipe was broken or the input failed */
			if (timedInput.failedInput == true) {
				throw new Exception();
			}
			/* see if the input operation just timed out */
			if (timedInput.timedOut == false){
				MainFrame.statsPanel.logText += playerName + " thought for " + ((double)(stopTime - startTime)/1000) + " sec\n"; 
				moveInt = playerProcess.nextMove;
			} else {
				MainFrame.statsPanel.logText += playerName + " timed out on input!\n";
				MainFrame.statsPanel.logText += otherPlayerName + " automatically wins!\n";
				return otherPlayerID;
			}
			MainFrame.statsPanel.logText += playerName + " sent me: " + moveInt + "\n";
		} catch (Exception e) {
			MainFrame.statsPanel.logText += playerName + ": Broken pipe!\n";
			MainFrame.statsPanel.logText += otherPlayerName + " automatically wins.\n";
			return otherPlayerID;
		}
		// Step 2: try to apply that move
		try{
			MainFrame.viewerMap.takeAction(playerID, moveInt);
			MainFrame.statsPanel.logText += "Recorded player's move!\n";
		} catch (Exception e){
			MainFrame.statsPanel.logText += playerName + " made a mistake and process will be killed.\n";
			MainFrame.statsPanel.logText += e.getMessage() + "\n";
			MainFrame.statsPanel.logText += otherPlayerName + " automatically wins.\n";
			return otherPlayerID;
		}
		// Step 3: check if the game is over
		int outcome = MainFrame.viewerMap.refreshHighlight(); 
		if (outcome != GameMap.EMPTY){
			MainFrame.statsPanel.logText += playerName + " wins and both processes will be killed.\n";
			return outcome;
		}
		// Step 4: send move to the other process
		try{
			otherPlayerProcess.sendMove(moveInt);
		} catch (Exception e) {
			MainFrame.statsPanel.logText += otherPlayerName + ": Broken pipe!\n";
			MainFrame.statsPanel.logText += playerName + " automatically wins.\n";
			return playerID;
		}
		// Inform the runner thread that the game should continue
		return GameMap.EMPTY;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == player1SourceCheckBox) {
			if (MainFrame.lastSimulatedStep != -1) {
				JOptionPane.showMessageDialog(owner, "Simulation already started!\n\nClick Reset");
				player1SourceCheckBox.setSelected(false);
			} else {
				if (player1SourceCheckBox.isSelected()){
					int returnValue = fileChooser.showOpenDialog(null);
					if (returnValue == JFileChooser.APPROVE_OPTION) {
						File inputFile = fileChooser.getSelectedFile();
						player1Path.setText(inputFile.getPath());
					} else {
						player1SourceCheckBox.setSelected(false);
						player1Path.setText(GParam.HUMAN_PLAYER_PATH);
					}
				} else {
					player1Path.setText(GParam.HUMAN_PLAYER_PATH);
				}
			}
		} else if (e.getSource() == player2SourceCheckBox) {
			if (MainFrame.lastSimulatedStep != -1) {
				JOptionPane.showMessageDialog(owner, "Simulation already started!\n\nClick Reset");
				player2SourceCheckBox.setSelected(false);
			} else {
				if (player2SourceCheckBox.isSelected()) {
					int returnValue = fileChooser.showOpenDialog(null);
					if (returnValue == JFileChooser.APPROVE_OPTION) {
						File inputFile = fileChooser.getSelectedFile();
						player2Path.setText(inputFile.getPath());
					} else {
						player2SourceCheckBox.setSelected(false);
						player2Path.setText(GParam.HUMAN_PLAYER_PATH);
					}
				} else {
					player2Path.setText(GParam.HUMAN_PLAYER_PATH);
				}
			}
		} else if (e.getSource() == stopButton) {
			resetAll("");
		} else if (e.getSource() == startButton) {
			try {
				// This only works if the game has not been started already
				if (MainFrame.lastSimulatedStep != -1){
					return;
				}
				// Try starting processes for the players
				if (MainFrame.buttons.player1Path.getText().equals(GParam.HUMAN_PLAYER_PATH)){
					MainFrame.player1process = new HumanPlayer(MainFrame.depiction);
				} else {
					try {
						MainFrame.player1process = new AIPlayer(MainFrame.buttons.player1Path.getText(), true);
					} catch (Exception startProcessException) {
						throw new Exception ("Error! Could not start player 1 because:\n" + startProcessException.getMessage());
					}
				}
				// Try communicating the difficulty to them
				try {
					MainFrame.player1process.sendMove(GParam.DIFFICULTY);
				} catch (Exception sendMoveException) {
					throw new Exception ("Error! Could not send difficulty information to player 1 because:\n" + sendMoveException.getMessage());
				}
				if (MainFrame.buttons.player2Path.getText().equals(GParam.HUMAN_PLAYER_PATH)){
					MainFrame.player2process = new HumanPlayer(MainFrame.depiction);
				} else {
					try {
						MainFrame.player2process = new AIPlayer(MainFrame.buttons.player2Path.getText(), false);
					} catch (Exception startProcessException) {
						throw new Exception ("Error! Could not start player 2 because:\n" + startProcessException.getMessage());
					}
				}
				// Try communicating the difficulty to them
				try {
					MainFrame.player2process.sendMove(GParam.DIFFICULTY);
				} catch (Exception sendMoveException) {
					throw new Exception ("Error! Could not send difficulty information to player 2 because:\n" + sendMoveException.getMessage());
				}
				if (MainFrame.run == null){
					MainFrame.run = new Thread(this);
					MainFrame.run.start();
				}
			} catch (Exception exception) {
				MainFrame.statsPanel.logText += exception.getMessage();
				resetAll("");
			}
		} else if (e.getSource() == saveButton) {
			if (MainFrame.lastSimulatedStep == -1) {
				JOptionPane.showMessageDialog(owner, "No game was recently played!");
			} else {
				int returnValue = fileChooser.showSaveDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					try {
						File outputFile = fileChooser.getSelectedFile();
						PrintWriter out = new PrintWriter(new FileOutputStream(outputFile));
						out.write(MainFrame.statsPanel.logText);
						out.close();
					} catch (IOException exception) {
						JOptionPane.showMessageDialog(null, "Could not save!");
					}
				} 
			} 
		} else if (e.getSource() == disableTimeout) {
			if (MainFrame.lastSimulatedStep != -1) {
				JOptionPane.showMessageDialog(owner, "Game already started!");
				disableTimeout.setSelected(!disableTimeout.isSelected());
			} else {
				/* it's ok if no game has started */
			}
		}
	}
}

class RemoveWhiteFilter extends RGBImageFilter {

	int first = 0x00FFFFFF;
	
	@Override
	public int filterRGB(int x, int y, int rgb) {
		if (rgb == Color.white.getRGB()){
			return 0;
		}
		return rgb;		
	}
	
}

public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	
	static GameCanvas depiction;
	static Canvas displayBuffer;
	
	static JLabel stats; 
	static StatsPanel statsPanel;
	static ButtonBar buttons;
	
	Thread animatie;
	static Thread run;
	
	static int lastSimulatedStep;
	static long timePlayer1;
	static long timePlayer2;
	static GameMap viewerMap;
	static Player player1process;
	static Player player2process;
	
	public static Map<String,Image> sprite;
	
	MainFrame() {
		super("Tema 2 - Viewer");
		// Set the window parameters
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		// Set the menu bar
		this.setJMenuBar(new CustomMenu(this));
		// Create the layout
		this.getContentPane().setLayout(new BorderLayout());
		MainFrame.buttons = new ButtonBar(this);
		this.getContentPane().add(buttons,BorderLayout.NORTH);
		// Create a double buffer an animation that constantly draws to the second one
		depiction = new GameCanvas(this);
		depiction.setSize(new Dimension(GParam.FRAME_WIDTH,GParam.FRAME_HEIGHT));
		displayBuffer = new GameCanvas(this);
		displayBuffer.setSize(new Dimension(GParam.FRAME_WIDTH,GParam.FRAME_HEIGHT));
		animatie = new Animatie();
		animatie.start();
		this.getContentPane().add(displayBuffer, BorderLayout.CENTER);
		// Create the statistics pane
		statsPanel = new StatsPanel(this);
		this.getContentPane().add(statsPanel, BorderLayout.EAST);
		// Consider last simulated step to be -1
		MainFrame.lastSimulatedStep = -1;
		// Load the textures from the files
		loadSprites();
		// Make the status bar
		stats = new JLabel("Stats: ");
		this.getContentPane().add(stats, BorderLayout.SOUTH);
		// Make the window visible at the center of the screen
		//this.setResizable(false);
		this.pack();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(new Point((screenSize.width-this.getSize().width)/2,(screenSize.height-this.getSize().height)/2));
		this.setVisible(true);
	}
	
	public void loadSprites() {
		MainFrame.sprite = new TreeMap<String,Image>();
		try {
			ImageProducer producer;
			ImageFilter filter = new RemoveWhiteFilter();
			for (int i = 0; i < GParam.availableTextures.length; i++){
			    InputStream fileName = MainFrame.class.getClassLoader().getResourceAsStream("Img/" + GParam.availableTextures[i] + ".png");
				producer = new FilteredImageSource(ImageIO.read(fileName).getSource(), filter);
				MainFrame.sprite.put(GParam.availableTextures[i], createImage(producer));
			}
		} catch (Exception e){
			JOptionPane.showMessageDialog(null, "Error! Missing texture files!\n" + e.getMessage());
			System.exit(0);
		}
	}
	
	public void exitGracefully(){
		System.exit(0);
	}
	
	public String getAboutInformation() {
		return "Tema 2 - Viewer\nVersion: 1.3\n> from 1.2: Added possibility to run java AIs on Windows\n> from 1.1: Added Windows AIs\n> from 1.0: Added 'Disable timeout' button.\nPlease send any bug reports to adrian.scoica@gmail.com\n\nPlease go to http://elf.cs.pub.ro/~pa for any updates!"; 
	}
	
	public static void main (String[] args){
		new MainFrame();
	}
	
}
