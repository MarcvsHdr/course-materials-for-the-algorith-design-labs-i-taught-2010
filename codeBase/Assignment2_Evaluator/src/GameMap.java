import java.awt.*;
import java.util.Vector;

public class GameMap {
	
	// The actual internal representation of the game map
	byte[][] gameMap;
	byte[][] highlight;
	boolean isInitial;
	Vector<byte[][]> allStates;
	
	public final static byte EMPTY = 0;
	public final static byte FIRST_PLAYER = 1;
	public final static byte SECOND_PLAYER = 2;
	
	private static int[] dirX = { 0, -1, -1, -1};
	private static int[] dirY = {-1, -1,  0,  1};
	
	public void takeAction(byte player, int column) throws Exception {
		// Look for a place to drop the chip
		if (column < 0 || column >= GParam.MAP_WIDTH){
			throw new Exception("Invalid Move: Invalid column for dropping chip: " + column);
		}
		int i;
		for (i = GParam.MAP_HEIGHT - 1; i >= 0; i--){
			if (gameMap[i][column] == EMPTY){
				break;
			}
		}
		if (i == -1){
			throw new Exception("Invalid Move: Column " + column + " is already full!");
		}
		// Check if the player is a valid one
		if (player != FIRST_PLAYER && player != SECOND_PLAYER){
			throw new Exception("Invalid Move: Player " + player + " does not exist!");
		}
		// Place the move and return
		gameMap[i][column] = player;
	}
	
	public int refreshHighlight(){
		// Step 1: erase all former highlight
		for (int i = 0; i < GParam.MAP_HEIGHT; i++){
			for (int j = 0; j < GParam.MAP_WIDTH; j++){
				highlight[i][j] = 0;
			}
		}
		// Step 2: iterate the map and check for another highlight
		for (int i = 0; i < GParam.MAP_HEIGHT; i++){
			for (int j = 0; j < GParam.MAP_WIDTH; j++){
				for (int dirIndex = 0; dirIndex < 4 && gameMap[i][j] != GameMap.EMPTY; dirIndex++){
					int hits = 0;
					for (int step = 0; step < 4; step++){
						if (i + dirX[dirIndex] * step >= 0 && i + dirX[dirIndex] * step < GParam.MAP_HEIGHT && j + dirY[dirIndex] * step >= 0 && j + dirY[dirIndex] * step < GParam.MAP_WIDTH){
							if (gameMap[i][j] == gameMap[i+dirX[dirIndex]*step][j+dirY[dirIndex]*step]){
								hits++;
							}
						}
					}
					if (hits == 4){
						// Step 3: if you find a hit, then iterate again and fill it in and return
						for (int step = 0; step < 4; step++){
							highlight[i + dirX[dirIndex] * step][j + dirY[dirIndex] * step] = gameMap[i][j];
						}
						return gameMap[i][j];
					}
				}
			}
		}
		// Step 3: if no hits were made, then return empty
		return GameMap.EMPTY;
	}
	
	public Color getHighlight(int i, int j){
		switch(highlight[i][j]){
		case GameMap.FIRST_PLAYER:
			return new Color((float)1.0, (float)0.0, (float)0.0, (float)0.25);
		case GameMap.SECOND_PLAYER:
			return new Color((float)0.0, (float)1.0, (float)0.0, (float)0.25);
		default:
			return null;
		}
	}
	
	public GameMap() {
		// Initializers and allocators
		this.isInitial = true;
		gameMap = new byte[GParam.MAP_HEIGHT][GParam.MAP_WIDTH];
		highlight = new byte[GParam.MAP_HEIGHT][GParam.MAP_WIDTH];
		// Fill the game map with initial values
		for (int i = 0; i < GParam.MAP_HEIGHT; i++){
			for (int j = 0; j < GParam.MAP_WIDTH; j++){
				gameMap[i][j] = GameMap.EMPTY;
				highlight[i][j] = GameMap.EMPTY;
			}
		}
		// Start the log history
		allStates = new Vector<byte[][]>();
		allStates.add(gameMap);
	}
}
