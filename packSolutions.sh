#!/bin/bash

## Written by: Adrian Scoica
## For any comments, questions and suggestions:
## 	adrian [dot] scoica [at] gmail [dot] com

EXPECTED_ARGS=1
E_BADARGS=1

if [ $# -lt 1 ]
	then
		echo "Incorrect number of arguments!"
		echo "Usage: `basename $0` <files>"
		exit $E_BADARGS
fi

for i in $@
	do
		echo "Now processing: ${i}"
		vim -c "normal G=gg" -c "%substitute/\t/  /g" -c "wq" ${i}
		echo "     ... Done."
	done

echo ""
echo "Done processing"
