.PHONY: pack clean scheletCodPA.zip

scheletCodPA.zip:
	cd codeBase/C++/doc; make clean; make
	cd codeBase/Java/doc; make clean; make
	zip -r scheletCodPA.zip codeBase/*

# Ca sa poti face deploy, trebuie sa ai cheie publica in contul 
#	pa@elf.cs.pub.ro
deploy: scheletCodPA.zip
	scp scheletCodPA.zip pa@elf.cs.pub.ro:~/public_html

clean:
	rm *.zip

